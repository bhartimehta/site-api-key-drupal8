<?php

namespace Drupal\site_config\Controller;

use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class SiteConfigController{

       public function content($prev_site_api_key, NodeInterface $node){

        // Site API Key saved value
        $site_api_key_saved_value = \Drupal::config('site.configuration')->get('siteapikey');

        // Condition to check node is a page, the configuration key is set and matches with the submit key value
        if($node->getType() == 'page' && $site_api_key_saved_value != 'No API Key yet' && $site_api_key_saved_value == $prev_site_api_key){

            // Json representation of the node
            return new JsonResponse($node->toArray(), 200, ['Content-Type'=> 'application/json']);
        }

        // Access denied
        return new JsonResponse(array("error" => "access denied"), 401, ['Content-Type'=> 'application/json']);
    }
}